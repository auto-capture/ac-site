<?php

/**
 * Project: auto-capture-site
 * Created by: WinXaito
 * Date: 14.04.2017
 */
class Ac_Screenshot{
    private $_id;
    private $_user_id;
    private $_uniqid;
    private $_size;
    private $_created_at;
    private $_updated_at;
    private $_status;

    /**
     * Ac_Screenshot constructor.
     * @param $_id
     * @param $_user_id
     * @param $_uniqid
     * @param $_size
     * @param $_created_at
     * @param $_updated_at
     * @param $_status
     */
    public function __construct($_id, $_user_id, $_uniqid, $_size, $_created_at, $_updated_at, $_status){
        $this->_id = $_id;
        $this->_user_id = $_user_id;
        $this->_uniqid = $_uniqid;
        $this->_size = $_size;
        $this->_created_at = $_created_at;
        $this->_updated_at = $_updated_at;
        $this->_status = $_status;
    }

    /**
     * @return mixed
     */
    public function getId(){
        return $this->_id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id){
        $this->_id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId(){
        return $this->_user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id){
        $this->_user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getUniqid(){
        return $this->_uniqid;
    }

    /**
     * @param mixed $uniqid
     */
    public function setUniqid($uniqid){
        $this->_uniqid = $uniqid;
    }

    /**
     * @return mixed
     */
    public function getSize(){
        return $this->_size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size){
        $this->_size = $size;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt(){
        return $this->_created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at){
        $this->_created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt(){
        return $this->_updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at){
        $this->_updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getStatus(){
        return $this->_status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status){
        $this->_status = $status;
    }
}