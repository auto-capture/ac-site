<?php

/**
 * Project: auto-capture-site
 * Created by: WinXaito
 * Date: 14.04.2017
 */
class Ac_Install{
    //Langue de l'interface
    private static $_lang;

    //Nom d'utilisateur de l'administrateur
    private static $_adminname;

    //Mot de passe de l'administrateur
    private static $_adminpass;

    //Email de l'administrateur
    private static $_adminemail;

    //Limite d'espace disque générale
    private static $_quotas;

    //Autorise l'enregistrement via l'interface
    private static $_register;

    //Limite espace disque par utilisateur
    private static $_userquotas;

    const INSTALL_1 = 1;
    const INSTALL_2 = 2;
    const INSTALL_3 = 3;
    const INSTALL_4 = 4;
    const INSTALL_5 = 5;

    public static function isInstallComplete(){
        return true;
    }

    public static function getInstallPage(){
        return Ac_Install::INSTALL_1;
    }

    //-----------------------------------------//

    /**
     * @return mixed
     */
    public static function getLang(){
        return self::$_lang;
    }

    /**
     * @param mixed $lang
     */
    public static function setLang($lang){
        self::$_lang = $lang;
    }

    /**
     * @return mixed
     */
    public static function getAdminname(){
        return self::$_adminname;
    }

    /**
     * @param mixed $adminname
     */
    public static function setAdminname($adminname){
        self::$_adminname = $adminname;
    }

    /**
     * @return mixed
     */
    public static function getAdminpass(){
        return self::$_adminpass;
    }

    /**
     * @param mixed $adminpass
     */
    public static function setAdminpass($adminpass){
        self::$_adminpass = $adminpass;
    }

    /**
     * @return mixed
     */
    public static function getAdminemail(){
        return self::$_adminemail;
    }

    /**
     * @param $adminemail
     */
    public static function setAdminemail($adminemail){
        self::$_adminemail = $adminemail;
    }

    /**
     * @return mixed
     */
    public static function getQuotas(){
        return self::$_quotas;
    }

    /**
     * @param mixed $quotas
     */
    public static function setQuotas($quotas){
        self::$_quotas = $quotas;
    }

    /**
     * @return mixed
     */
    public static function getRegister(){
        return self::$_register;
    }

    /**
     * @param mixed $register
     */
    public static function setRegister($register){
        self::$_register = $register;
    }

    /**
     * @return mixed
     */
    public static function getUserquotas(){
        return self::$_userquotas;
    }

    /**
     * @param mixed $userquotas
     */
    public static function setUserquotas($userquotas){
        self::$_userquotas = $userquotas;
    }
}