<?php

/**
 * Project: auto-capture-site
 * Created by: WinXaito
 * Date: 14.04.2017
 */
class Ac_ScreenshotManager{
    /**
     * @param $uniqid
     * @return Ac_Screenshot|null
     */
    public static function get($uniqid){
        $q = Wx_Query::query(
            '
                SELECT *
                FROM screenshot
                WHERE uniqid = :uniqid
            ',
            [
                'uniqid' => $uniqid,
            ]
        );

        $data = $q->fetch();

        if($data == null)
            return null;

        return new Ac_Screenshot(
            $data['id'],
            $data['user_id'],
            $data['uniqid'],
            $data['size'],
            $data['created_at'],
            $data['updated_at'],
            $data['status']
        );
    }

    /**
     * @param Wx_User $user
     * @return array
     */
    public static function getUserScreenshot(Wx_User $user){
        $q = Wx_Query::query(
            '
                SELECT *
                FROM screenshot
                WHERE user_id = :user_id
            ',
            [
                'user_id' => $user->getId(),
            ]
        );

        $return = [];
        while($data = $q->fetch()){
            $return[] = new Ac_Screenshot(
                $data['id'],
                $data['user_id'],
                $data['uniqid'],
                $data['size'],
                $data['created_at'],
                $data['updated_at'],
                $data['status']
            );
        }

        return $return;
    }

    /**
     * @param Ac_Screenshot $screenshot
     * @return bool
     */
    public static function add(Ac_Screenshot $screenshot){
        $q = Wx_Query::query(
            '
                  INSERT INTO screenshot
                  (user_id, uniqid, size, created_at, updated_at, status)
                  VALUES
                  (:user_id, :uniqid, :size, :created_at, :updated_at, :status)
            ',
            [
                'user_id' => $screenshot->getUserId(),
                'uniqid' => $screenshot->getUniqid(),
                'size' => $screenshot->getSize(),
                'created_at' => $screenshot->getCreatedAt(),
                'updated_at' => 0,
                'status' => $screenshot->getStatus()
            ]
        );

        return $q->errorCode() == 0 ? true : false;
    }

    /**
     * @param Ac_Screenshot $screenshot
     * @return bool
     */
    public static function update(Ac_Screenshot $screenshot){
        $screenshot->setUpdatedAt(time());

        $q = Wx_Query::query(
            '
                UPDATE screenshot
                SET user_id = :user_id,
                    uniqid = :uniqid,
                    size = :size,
                    created_at = :created_at,
                    updated_at = :updated_at,
                    status = :status
            ',
            [
                'user_id' => $screenshot->getUserId(),
                'uniqid' => $screenshot->getUniqid(),
                'size' => $screenshot->getSize(),
                'created_at' => $screenshot->getCreatedAt(),
                'updated_at' => $screenshot->getUpdatedAt(),
                'status' => $screenshot->getStatus(),
            ]
        );

        return $q->errorCode() == 0 ? true : false;
    }

    /**
     * @param Ac_Screenshot $screenshot
     * @return bool
     */
    public static function remove(Ac_Screenshot $screenshot){
        $screenshot->setStatus('rm');
        return self::update($screenshot);
    }

    //---------------------------------------------------------//

    /**
     * @param Ac_Screenshot $screenshot
     * @param $tmpName
     * @return bool
     */
    public static function upload(Ac_Screenshot $screenshot, $tmpName){
        $path = __DIR__.'/../../media/users/'.$screenshot->getUserId().'/screenshot/';
        $pathFilename = __DIR__.'/../../media/users/'.$screenshot->getUserId().'/screenshot/'.$screenshot->getUniqid().'.ac';

        if(!file_exists($path))
            if(!mkdir($path, 0777, true))
                return false;

        if(move_uploaded_file($tmpName, $pathFilename)){
            self::add($screenshot);
            return true;
        }

        return false;
    }
}