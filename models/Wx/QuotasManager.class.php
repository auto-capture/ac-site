<?php

/**
 * Project: promgr-site
 * Created by: WinXaito
 * Date: 25.03.2017
 */
class Wx_QuotasManager{
    /**
     * @param $user_id
     * @return mixed
     */
    public static function getUserSize($user_id){
        $q = Wx_Query::query(
            '
                SELECT size
                FROM quotas
                WHERE type = :type
                AND user_id = :user_id
            ',
            [
                'type' => 'user',
                'user_id' => $user_id,
            ]
        );

        $data = $q->fetch()[0];

        if($data == null)
            return 0;

        return $data;
    }

    /**
     * @param Wx_Project $project
     * @return mixed
     */
    public static function getProjectSize(Wx_Project $project){
        $q = Wx_Query::query(
            '
                SELECT size
                FROM quotas
                WHERE type `project`
                AND project_id = :project_id
            ',
            [
                'project_id' => $project->getId(),
            ]
        );

        return $q->fetch();
    }

    /**
     * @param $user_id
     * @return array
     */
    public static function getProjectsSize($user_id){
        $q = Wx_Query::query(
            '
                SELECT size, project_id
                FROM quotas
                WHERE type = :type
                AND user_id = :user_id
            ',
            [
                'type' => 'project',
                'user_id' => $user_id,
            ]
        );

        $return = [];
        while($data = $q->fetch()){
            $return[] = [
                'project_id' => $data['project_id'],
                'size' => $data['size'],
            ];
        }

        return $return;
    }

    /**
     * @param Wx_Project $project
     * @return bool
     */
    public static function initProject(Wx_Project $project){
        $q = Wx_Query::query(
            '
                INSERT INTO quotas
                (type, project_id, user_id, size)
                VALUES
                (:type, :project_id, :user_id, :size)
            ',
            [
                'type' => 'project',
                'project_id' => $project->getId(),
                'user_id' => $project->getOwner(),
                'size' => 0,
            ]
        );

        return $q->errorCode() == 0 ? true : false;
    }

    /**
     * @param Wx_User $user
     * @return bool
     */
    public static function initUser(Wx_User $user){
        $q = Wx_Query::query(
            '
                INSERT INTO quotas
                (type, project_id, user_id, size)
                VALUES
                (:type, :project_id, :user_id, :size)
            ',
            [
                'type' => 'user',
                'project_id' => 0,
                'user_id' => $user->getId(),
                'size' => 0,
            ]
        );

        return $q->errorCode() == 0 ? true : false;
    }

    /**
     * @param Wx_Project $project
     * @param null $project_id
     * @return bool
     */
    public static function update(Wx_Project $project=null, $project_id=null){
        if($project == null)
            $project = Wx_ProjectManager::getId($project_id);

        $q = Wx_Query::query(
            '
                UPDATE quotas
                SET size = :size,
                    user_id = :user_id
                WHERE type = :type
                AND project_id = :project_id
            ',
            [
                'type' => 'project',
                'size' => Wx_Std_FilesManager::getProjectSize($project),
                'project_id' => $project->getId(),
                'user_id' => $project->getOwner(),
            ]
        );

        self::updateUser($project->getOwner());
        return $q->errorCode() == 0 ? true : false;
    }

    /**
     * @param $user_id
     * @return int
     */
    private static function getProjectsUserSize($user_id){
        $q = Wx_Query::query(
            '
                SELECT size
                FROM quotas
                WHERE type = :type
                AND user_id = :user_id
            ',
            [
                'type' => 'project',
                'user_id' => $user_id,
            ]
        );

        $return = 0;
        while($data = $q->fetch()){
            $return += $data['size'];
        }

        return $return;
    }

    /**
     * @param $user_id
     * @return bool
     */
    private static function updateUser($user_id){
        $q = Wx_Query::query(
            '
                UPDATE quotas
                SET size = :size
                WHERE type = :type
                AND user_id = :user_id
            ',
            [
                'type' => 'user',
                'size' => self::getProjectsUserSize($user_id),
                'user_id' => $user_id,
            ]
        );

        return $q->errorCode() == 0 ? true : false;
    }

    public static function getNumberProjectsUser(Wx_User $user){
        $q = Wx_Query::query(
            '
                SELECT COUNT(id)
                FROM projects
                WHERE owner = :owner
            ',
            [
                'owner' => $user->getId(),
            ]
        );

        return $q->fetch()[0];
    }
}