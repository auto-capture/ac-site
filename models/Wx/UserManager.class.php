<?php
/**
 * Project: paneldev
 * License: GPL3.0
 * User: WinXaito
 */


class Wx_UserManager{
    public static $sql_whitelist = ['id', 'name', 'profil_public', 'grade'];

    /**
     * @param Wx_User $user
     * @return bool
     */
    public static function add(Wx_User $user){
        $q = Wx_Query::query(
            "
                INSERT INTO users
                (name, password, grade)
                VALUES
                (:name, :password, :grade)
            ",
            [
                'name' => $user->getName(),
                'password' => $user->getPassword(),
                'grade' => $user->getGrade(),
            ]
        );

        if($q->errorCode() == 0) {
            $user->setId(Wx_Query::getLastInsertedId());
            Wx_QuotasManager::initUser($user);
        }

        return $q->errorCode() == 0 ? true : false;
    }

    /**
     * @param Wx_User $user
     * @return bool
     */
    public static function update(Wx_User $user){
        $q = Wx_Query::query(
            "
                UPDATE users
                SET name = :name,
                    password = :password,
                    grade = :grade,
                WHERE id = :id
            ",
            [
                'id' => $user->getId(),
                'name' => $user->getName(),
                'password' => $user->getPassword(),
                'grade' => $user->getGrade(),
            ]
        );

        return $q->errorCode() == 0 ? true : false;
    }


    /**
     * @param $username
     * @return Wx_User
     */
    public static function getUserByName($username){
        $q = Wx_Query::query(
            "
                SELECT *
                FROM users
                WHERE name = :name
            ",
            [
                'name' => $username,
            ]
        );

        $result = $q->fetch();

        if($result){
            $users = new Wx_User(
                $result['name'],
                $result['password'],
                $result['grade'],
                $result['profil_public'],
                $result['id']
            );
            return $users;
        }else{
            return null;
        }
    }

    /**
     * @param $userid
     * @return bool|Wx_User
     */
    public static function getUserById($userid){
        $q = Wx_Query::query(
            "
                SELECT *
                FROM users
                WHERE id = :id
            ",
            [
                'id' => $userid,
            ]
        );

        $result = $q->fetch();

        if($result){
            $users = new Wx_User(
                $result['name'],
                $result['password'],
                $result['grade'],
                $result['profil_public'],
                $result['id']
            );
            return $users;
        }else{
            return false;
        }
    }

    /**
     * @param string $orderby
     * @return array
     */
    public static function getAllUsers($orderby = 'id'){
        if(!in_array($orderby, self::$sql_whitelist))
            $orderby = 'id';

        $q = Wx_Query::query(
            '
                SELECT *
                FROM users
                ORDER BY '.$orderby.'
            ', []
        );


        $return = [];
        while($data = $q->fetch()){
            $return[] = new Wx_User(
                $data['name'],
                $data['password'],
                $data['grade'],
                $data['profil_public'],
                $data['id']
            );
        }

        return $return;
    }

    /**
     * @return array
     */
    public static function getAllPublicUsers(){
        $q = Wx_Query::query(
            '
                SELECT *
                FROM users
                WHERE profil_public = TRUE 
            ', []
        );

        $return = [];
        $i = 0;
        while($data = $q->fetch()){
            $return[$i] = new Wx_User(
                $data['name'],
                $data['password'],
                $data['grade'],
                $data['profil_public'],
                $data['id']
            );
            $i++;
        }

        return $return;
    }

    /**
     * @param $username
     * @return array
     */
    public static function checkExistUsername($username){
        $return = [
            'success' => true,
            'message' => '',
        ];

        if(strlen($username) < 3 || strlen($username) > 15){
            $return = [
                'success' => false,
                'message' => 'Le nom d\'utilisateur doit être compris en 3 et 15 caractères',
            ];
        }else{
            $existUser = self::getUserByName($username);

            if($existUser != null){
                $return = [
                    'success' => false,
                    'message' => 'Le nom d\'utilisateur choisi n\'est pas disponible',
                ];
            }
        }

        return $return;
    }

    /**
     * @param $password
     * @param $confirmPassword
     * @return array
     */
    public static function checkPassword($password, $confirmPassword){
        $return = [
            'success' => true,
            'message' => '',
        ];

        if(strlen($password) < 8){
            $return = [
                'success' => false,
                'message' => 'Le mot de passe doit faire au minimum une longueur de 8 caractères',
            ];
        }elseif($password != $confirmPassword){
            $return = [
                'success' => false,
                'message' => 'Le mot de passe ne correspond pas à sa confirmation',
            ];
        }

        return $return;
    }

    /**
     * @param $email
     * @return array
     */
    public static function checkEmail($email){
        $return = [
            'success' => true,
            'message' => '',
        ];

        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $return = [
                'success' => false,
                'message' => 'Le format de l\'adresse email n\'est pas correct',
            ];
        }

        return $return;
    }

    /**
     * @param $password
     * @return string
     */
    public static function getPasswordHash($password){
        $password = PASSWORD_HASH_PREFIX.$password.PASSWORD_HASH_SUFFIX;
        return sha1($password);
    }
}