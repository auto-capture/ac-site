<?php
/**
 * Project: paneldev
 * License: GPL3.0
 * User: WinXaito
 */

class Wx_User{
    private $_id;
    private $_name;
    private $_password;
    private $_grade;
    private $_profil_public;

    const GRADE_INITIAL = 1;
    const GRADE_MODERATOR = 3;
    const GRADE_ADMINISTRATOR = 4;

    /**
     * @param $name
     * @param $password
     * @param $grade
     * @param $profil_public
     * @param int $id
     */
    public function __construct($name, $password, $grade, $profil_public, $id=0){
        $this->_name = $name;
        $this->_password = $password;
        $this->_grade = $grade;
        $this->_profil_public = $profil_public;
        $this->_id = $id;
    }

    /**
     * @return int
     */
    public function getId(){
        return $this->_id;
    }

    /**
     * @param int $id
     */
    public function setId($id){
        $this->_id = $id;
    }

    /**
     * @return mixed
     */
    public function getName(){
        return $this->_name;
    }

    /**
     * @param $name
     */
    public function setName($name){
        $this->_name = $name;
    }

    /**
     * @return mixed
     */
    public function getPassword(){
        return $this->_password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password){
        $this->_password = $password;
    }

    /**
     * @return mixed
     */
    public function getGrade(){
        return $this->_grade;
    }

    /**
     * @param $grade
     */
    public function setGrade($grade){
        $this->_grade = $grade;
    }

    /**
     * @return boolean
     */
    public function isProfilPublic(){
        return $this->_profil_public;
    }

    /**
     * @param mixed $profil_public
     */
    public function setProfilPublic($profil_public){
        $this->_profil_public = $profil_public;
    }

    /**
     * @return array
     */
    public function getSession(){
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'grade' => $this->getGrade(),
        ];
    }

    /**
     * @return bool
     */
    public function isModerator(){
        return $this->getGrade() == self::GRADE_MODERATOR ? true : false;
    }

    /**
     * @return bool
     */
    public function isAdministrator(){
        return $this->getGrade() == self::GRADE_ADMINISTRATOR ? true : false;
    }

    public function deconnect(){
        Wx_Session::destroy();
        session_destroy();
    }
}