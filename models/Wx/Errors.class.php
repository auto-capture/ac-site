<?php
/**
 * Project: paneldev
 * License: GPL3.0
 * User: WinXaito
 */

require_once __DIR__.'/../../controllers/errors/errors_headers.php';

class Wx_Errors{
    private static $_twig;
    private static $_error;

    private static $_error_title;
    private static $_error_content;

    public function __construct(Twig_Environment $twig){
        self::$_twig = $twig;
    }

    /**
     * @param $error
     * @param bool $api
     */
    public static function setAndShowError($error, $api=false){
        $exc = new Exception("Error ".$error, $error);

        if(ENABLE_SENTRY) {
            if (Wx_Session::isAuthenticated()) {
                $extras = [
                    'server' => Wx_Server::getServer(),
                    'env' => Wx_Server::getEnvToString(),
                    'userid' => Wx_Session::getUser()->getId(),
                    'username' => Wx_Session::getUser()->getName(),
                    'usermail' => Wx_Session::getUser()->getEmail(),
                ];
            } else {
                $extras = [
                    'server' => Wx_Server::getServer(),
                    'env' => Wx_Server::getEnvToString(),
                ];
            }

            Wx_Sentry::getSentry()->captureException($exc, [
                    'extra' => $extras,
                ]
            );
        }

        self::setError($error);
        self::showError($api);
    }

    /**
     * @param $error
     */
    public static function setError($error){
        Wx_Errors::$_error = $error;
        Wx_Errors::textErrors();
    }

    /**
     * void
     * @param $api
     */
    public static function showError($api){
        self::render($api);
        exit();
    }

    /**
     * @param $error
     */
    public static function setAndShowErrorAjax($error){
        Wx_Errors::setError($error);
        Wx_Ajax::render([
            'status' => 'error',
            'code' => Wx_Errors::$_error,
            'title' => Wx_Errors::$_error_title,
            'content' => Wx_Errors::$_error_content,
        ]);
    }

    private static function textErrors(){
        switch(Wx_Errors::$_error){
            case 400:
                Wx_Errors::$_error_title = E400_title;
                Wx_Errors::$_error_content = E400_content;
                header($_SERVER['SERVER_PROTOCOL']." ".E400_header);
                break;
            case 401:
                Wx_Errors::$_error_title = E401_title;
                Wx_Errors::$_error_content = E401_content;
                header($_SERVER['SERVER_PROTOCOL']." ".E401_header);
                break;
            case 402:
                Wx_Errors::$_error_title = E402_title;
                Wx_Errors::$_error_content = E402_content;
                header($_SERVER['SERVER_PROTOCOL']." ".E402_header);
                break;
            case 403:
                Wx_Errors::$_error_title = E403_title;
                Wx_Errors::$_error_content = E403_content;
                header($_SERVER['SERVER_PROTOCOL']." ".E403_header);
                break;
            case 404:
                Wx_Errors::$_error_title = E404_title;
                Wx_Errors::$_error_content = E404_content;
                header($_SERVER['SERVER_PROTOCOL']." ".E404_header);
                break;
            case 500:
                Wx_Errors::$_error_title = E500_title;
                Wx_Errors::$_error_content = E500_content;
                header($_SERVER['SERVER_PROTOCOL']." ".E500_header);
                break;
            case 1000:
                Wx_Errors::$_error_title = E1000_title;
                Wx_Errors::$_error_content = E1000_content;
                header($_SERVER['SERVER_PROTOCOL']." ".E1000_header);
                break;
            default:
                Wx_Errors::$_error_title = "Erreur inconnu";
                Wx_Errors::$_error_content = "Une erreur inconnu est survenu";
        }
    }

    /**
     * @param $api
     */
    private static function render($api){
        if(!$api) {
            $breadcrum = new Wx_Breadcrum(
                false,
                [
                    'Accueil' => '',
                    'Erreur' => ''
                ]
            );

            echo self::$_twig->render('templates_pages/error/error.twig', [
                'breadcrum' => $breadcrum->getBreadcrum(),
                'title' => Wx_Errors::$_error_title,
                'content' => Wx_Errors::$_error_content,
            ]);
        }else{
            WxApi_Api::show([
                'error' => [
                    'title' => self::$_error_title,
                    'content' => self::$_error_content,
                    'code' => self::$_error,
                ]
            ]);
        }
    }
}