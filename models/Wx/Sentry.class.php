<?php

/**
 * Project: promgr-site
 * Created by: WinXaito
 * Date: 27.03.2017
 */

require_once __DIR__.'/../../sentry_dsn.php';

class Wx_Sentry{
    private static $client;

    public static function init(){
        if(ENABLE_SENTRY) {
            self::$client = new Raven_Client(SENTRY_DSN);
            self::$client->setEnvironment(Wx_Server::getEnvToString());

            $error_handler = new Raven_ErrorHandler(self::$client);
            $error_handler->registerExceptionHandler();
            $error_handler->registerErrorHandler();
            $error_handler->registerShutdownFunction();
        }
    }

    /**
     * @return Raven_Client
     */
    public static function getSentry(){
        return self::$client;
    }
}