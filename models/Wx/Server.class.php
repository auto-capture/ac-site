<?php

/**
 * Project: promgr-site
 * Created by: WinXaito
 * Date: 25.03.2017
 */
class Wx_Server{
    private static $env = Wx_Server::None;
    private static $server = null;
    private static $uri = null;

    const Prod = 0;
    const Preprod = 1;
    const Dev = 2;
    const LocalDev = 3;
    const None = 10;

    /**
     * @param array $servers
     */
    public static function init(array $servers){
        $host = $_SERVER['HTTP_HOST'];

        foreach($servers as $env => $serverUri){
            foreach($serverUri as $server => $uri){
                if($server == $host){
                    self::$env = $env;
                    self::$server = $server;
                    self::$uri = $uri;
                }
            }
        }

        if(self::$env == Wx_Server::None || self::$server == null)
            Wx_Errors::setAndShowError(1000);
    }

    /**
     * @return int
     */
    public static function getEnv(){
        return self::$env;
    }

    /**
     * @return string
     */
    public static function getEnvToString(){
        switch(self::$env){
            case Wx_Server::LocalDev:
                return 'LocalDev';
            case Wx_Server::Dev:
                return 'Dev';
            case Wx_Server::Preprod:
                return 'Preprod';
            case Wx_Server::Prod:
                return 'Prod';
            default:
                return 'None';
        }
    }

    /**
     * @return null
     */
    public static function getServer(){
        return self::$server;
    }

    /**
     * @return null
     */
    public static function getUri(){
        return self::$uri;
    }

    /**
     * @return bool
     */
    public static function isProd(){
        if(self::$env == Wx_Server::Prod)
            return true;

        return false;
    }

    /**
     * @return bool
     */
    public static function isPreProd(){
        if(self::$env == Wx_Server::Preprod)
            return true;

        return false;
    }

    /**
     * @return bool
     */
    public static function isDev(){
        if(self::$env == Wx_Server::Dev)
            return true;

        return false;
    }

    /**
     * @return bool
     */
    public static function isLocalDev(){
        if(self::$env == Wx_Server::LocalDev)
            return true;

        return false;
    }
}