<?php

/**
 * Project: auto-capture-site
 * Created by: WinXaito
 * Date: 09.04.2017
 */
class WxApi_Api{
    private static $_output = 'JSON';

    /**
     * @return mixed
     */
    public static function getOutput(){
        return self::$_output;
    }

    /**
     * @param mixed $output
     */
    public static function setOutput($output){
        switch($output){
            case 'xml':
                self::$_output = 'XML';
                break;
            case 'json':
                self::$_output = 'JSON';
                break;
            default:
                self::$_output = 'JSON';
        }
    }

    /**
     * @param $data
     */
    public static function show($data){
        if(self::$_output == 'XML'){
            header("Content-type: application/xml");
            $xml_user_info = new SimpleXMLElement("<?xml version=\"1.0\"?><root></root>");
            self::array_to_xml($data,$xml_user_info);
            echo $xml_user_info->asXML();
        }else{
            header("Content-type: application/json");
            echo json_encode($data);
            //echo '<form method="POST" action="" enctype="multipart/form-data"><input type="file" name="pic"/><input type="submit"/></form>';
        }
    }

    /**
     * @param $error
     */
    public static function setAndShowError($error){
        /** @var $apiErrors array */
        require_once __DIR__.'/../../api/apierrors.php';

        $data = [
            'status' => 'error',
            'code' => $error,
            'message' => $apiErrors[$error],
        ];

        WxApi_Api::show($data);
        exit;
    }

    /**
     * @param array $array
     * @param SimpleXMLElement $xml_user_info
     */
    private static function array_to_xml(array $array, SimpleXMLElement &$xml_user_info) {
        foreach($array as $key => $value) {
            if(is_array($value)) {
                if(!is_numeric($key)){
                    $subnode = $xml_user_info->addChild("$key");
                    self::array_to_xml($value, $subnode);
                }else{
                    $subnode = $xml_user_info->addChild("item$key");
                    self::array_to_xml($value, $subnode);
                }
            }else {
                $xml_user_info->addChild("$key",htmlspecialchars("$value"));
            }
        }
    }
}