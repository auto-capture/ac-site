<?php

/**
 * Project: auto-capture-site
 * Created by: WinXaito
 * Date: 16.04.2017
 */
class WxApi_Token{
    private $_user;
    private $_access_token;
    private $_refresh_token;

    /**
     * WxApi_Token constructor.
     * @param $_user Wx_User
     * @param $_access_token
     * @param $_refresh_token
     */
    public function __construct($_user, $_access_token, $_refresh_token){
        $this->_user = $_user;
        $this->_access_token = $_access_token;
        $this->_refresh_token = $_refresh_token;
    }

    /**
     * @return Wx_User
     */
    public function getUser(){
        return $this->_user;
    }

    /**
     * @param Wx_User $user
     */
    public function setUser($user){
        $this->_user = $user;
    }

    /**
     * @return mixed
     */
    public function getAccessToken(){
        return $this->_access_token;
    }

    /**
     * @param mixed $access_token
     */
    public function setAccessToken($access_token){
        $this->_access_token = $access_token;
    }

    /**
     * @return mixed
     */
    public function getRefreshToken(){
        return $this->_refresh_token;
    }

    /**
     * @param mixed $refresh_token
     */
    public function setRefreshToken($refresh_token){
        $this->_refresh_token = $refresh_token;
    }
}