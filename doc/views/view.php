<?php
/**
 * Project: auto-capture-site
 * Created by: WinXaito
 * Date: 17.04.2017
 */

/** @var $twig Twig_Environment */
require_once __DIR__ . '/../init.php';

$page = isset($_GET['page']) ? $_GET['page'] : '';


$docPath = __DIR__.'/../docfiles/'.$page;
if(!file_exists($docPath))
    Wx_Errors::setAndShowError(404);

$markdown = file_get_contents($docPath);
$parser = new Parsedown();
$content = $parser->parse($markdown);

echo $twig->render('base.twig', [
    'content' => $content,
]);