<?php
/**
 * Project: auto-capture-site
 * Created by: WinXaito
 * Date: 17.04.2017
 */

//Load environment file
/** @var $env array */
require_once __DIR__.'/../env.php';

//Load config file
if($env['env'] == 'prod')
    require_once __DIR__.'/../config.prod.php';
else
    require_once __DIR__.'/../config.dev.php';

require_once __DIR__.'/doc.config.php';

//Load composer autoloader
require_once __DIR__.'/../vendor/autoload.php';

//Init sentry
Wx_Sentry::init();

//Start session
if(session_id() == null)
    session_start();

//Twig
$loader = new Twig_Loader_Filesystem(__DIR__.'/views/');

if(TWIG_CACHE) {
    $twig = new Twig_Environment($loader, ['debug' => DEBUG, 'cache' => __DIR__ . '/../temp/twig_cache/doc/']);
}else {
    $twig = new Twig_Environment($loader, ['debug' => DEBUG, 'cache' => false]);
    $twig->addExtension(new Twig_Extension_Debug());
}

$twig->addGlobal('DOC_URL', DOC_URL);

//Init error
new Wx_Errors($twig);