<?php
/**
 * Project: auto-capture-site
 * Created by: WinXaito
 * Date: 09.04.2017
 */

//Api
define('OUTPUT', 'JSON');

//General
define('DEBUG', false);
define('TWIG_CACHE', false);

//User
define('PASSWORD_HASH_PREFIX', 'a8s(s92)sd2lsds2');
define('PASSWORD_HASH_SUFFIX', 'as83nd#as9Kdsl23');

//Url
define('URL_PATH', '');

//Database
define('DB_HOST', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'auto-capture');

//Sentry
define('ENABLE_SENTRY', false);