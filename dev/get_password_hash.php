<?php
/**
 * Project: auto-capture-site
 * Created by: WinXaito
 * Date: 17.04.2017
 */

require_once __DIR__.'/../vendor/autoload.php';

require_once __DIR__.'/../config.prod.php';

echo '
    <form method="POST" action="">
        <input type="text" name="passwd"/>
        <input type="submit" value="Envoyer"/>
    </form>
';

if(isset($_POST['passwd'])&&!empty($_POST['passwd']))
    echo '<br><br>Password: '.$_POST['passwd'].' ('.Wx_UserManager::getPasswordHash($_POST['passwd']).')';