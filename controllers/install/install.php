<?php
/**
 * Project: auto-capture-site
 * Created by: WinXaito
 * Date: 13.04.2017
 */

/** @var $twig Twig_Environment */
require_once __DIR__.'/../init.php';

if(Ac_Install::isInstallComplete())
    header('Location:/');

$page = isset($_GET['page'])&&is_numeric($_GET['page'])&&($_GET['page'] > 0 && $_GET['page'] < 5) ? $_GET['page'] : 1;

$params['continue'] = false;
switch($page){
    case 1:
        require_once __DIR__.'/install_1.php';

        if($params['continue'])
            header('Location:/install/2');

        break;
    case 2:
        require_once __DIR__.'/install_2.php';

        if($params['continue'])
            header('Location:/install/3');

        break;
    case 3:
        require_once __DIR__.'/install_3.php';

        if($params['continue'])
            header('Location:/install/4');

        break;
    case 4:
        require_once __DIR__.'/install_4.php';

        if($params['continue'])
            header('Location:/');

        break;
    default:
        Wx_Errors::setAndShowError(500);
}

echo $twig->render('templates_pages/install/install_'.$page.'.twig', []);