<?php
/**
 * Project: auto-capture-site
 * Created by: WinXaito
 * Date: 13.04.2017
 */

/** @var $twig Twig_Environment */
require_once __DIR__.'/../init.php';

if(!Ac_Install::isInstallComplete())
    header('Location:/install');

echo $twig->render('templates_pages/home/home.twig', []);