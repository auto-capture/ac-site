<?php
/**
 * Project: auto-capture-site
 * Created by: WinXaito
 * Date: 14.04.2017
 */

require_once __DIR__.'/../init.php';

$uniqid = isset($_GET['uniqid'])&&!empty($_GET['uniqid']) ? $_GET['uniqid'] : null;

if($uniqid == null)
    Wx_Errors::setAndShowError(404);

$screenshot = Ac_ScreenshotManager::get($uniqid);

$image_unavaible = __DIR__.'/../../assets/image/unavaible_image.png';

header('Content-Type: image/png');

if($screenshot != null) {
    $file_path = __DIR__ . '/../../media/users/' . $screenshot->getUserId() . '/screenshot/' . $screenshot->getUniqid() . '.ac';

    if (file_exists($file_path))
        readfile($file_path);
}

readfile($image_unavaible);