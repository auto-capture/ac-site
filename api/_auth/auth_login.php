<?php
/**
 * Project: auto-capture-site
 * Created by: WinXaito
 * Date: 17.04.2017
 */

require_once __DIR__.'/authentication.php';

$data = [
    'status' => 'ok',
    'code' => 1101,
    'message' => 'Connexion réussi'
];

WxApi_Api::show($data);