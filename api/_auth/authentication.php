<?php
/**
 * Project: auto-capture-site
 * Created by: WinXaito
 * Date: 16.04.2017
 */

require_once __DIR__.'/../init.php';

if(isset($_POST['username'], $_POST['password'])){
    if(!empty($_POST['username'])&&!empty($_POST['password'])){
        if(Wx_Session::getUser() == null)
            WxApi_Api::setAndShowError(1100);

        if(Wx_Session::getUser()->getPassword() != Wx_UserManager::getPasswordHash($_POST['password'])){
            WxApi_Api::setAndShowError(1100);
        }
    }else{
        WxApi_Api::setAndShowError(1000);
    }
}else{
    WxApi_Api::setAndShowError(1001);
}