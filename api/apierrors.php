<?php
/**
 * Project: auto-capture-site
 * Created by: WinXaito
 * Date: 16.04.2017
 */

$apiErrors = [
    403 => 'Vous n\'avez pas accès à la ressource demandé',
    404 => 'Ressource n\'ont trouvé',
    500 => 'Une erreur interne au serveur est survenu',

    1000 => 'Tous les champs nécessaire n\'ont pas été rempli',
    1001 => 'Certains paramètres sont manquants',

    1100 => 'Nom d\'utilisateur ou mot de passe incorrect',
];