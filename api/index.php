<?php
/**
 * Project: auto-capture-site
 * Created by: WinXaito
 * Date: 09.04.2017
 */

require_once __DIR__.'/init.php';

$data = [
    '_link' => [
        'current' => CURRENT_LINK,
        'api' => '/api',
        'doc' => '/doc',
        'ui' => '',
    ],
    'name' => 'Auto-capture API',
    'version' => '0.0.0',
];

WxApi_Api::show($data);