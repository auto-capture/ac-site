<?php
/**
 * Project: auto-capture-site
 * Created by: WinXaito
 * Date: 16.04.2017
 */

require_once __DIR__.'/../init.php';

Wx_Session::requireApiAuthentication();

if(isset($_GET['uniqid'])){
    $screenshot = Ac_ScreenshotManager::get($_GET['uniqid']);

    if($screenshot != null){
        if(!Wx_Session::getUser()->isAdministrator() || $screenshot->getUserId() != Wx_Session::getUser()->getId())
            WxApi_Api::setAndShowError(403);
    }
}else{
    $screenshot = Ac_ScreenshotManager::getUserScreenshot(Wx_Session::getUser());
}

if(empty($screenshot)){
    $data = [
        'status' => 'ok',
        'code' => 1200,
        'message' => 'Aucune capture d\'écran trouvé'
    ];
}else{
    $data = [];

    /** @var Ac_Screenshot $ss */
    foreach($screenshot as $ss){
        $data[] = [
            'id' => $ss->getId(),
            'uniqid' => $ss->getUniqid(),
            'size' => $ss->getSize(),
            'created_at' => $ss->getCreatedAt(),
            'updated_at' => $ss->getUpdatedAt(),
            'status' => $ss->getStatus(),
        ];
    }
}

WxApi_Api::show($data);