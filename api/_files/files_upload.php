<?php
/**
 * Project: auto-capture-site
 * Created by: WinXaito
 * Date: 10.04.2017
 */

require_once __DIR__.'/../init.php';

Wx_Session::requireApiAuthentication();

if(isset($_FILES)){
    if(isset($_FILES['file'])){
        $screenshot = new Ac_Screenshot(
            0,
            0,
            uniqid(),
            $_FILES['file']['size'],
            time(),
            0,
            'ok'
        );

        if(Ac_ScreenshotManager::upload($screenshot, $_FILES['file']['tmp_name'])){
            $api_show = [
                'status' => 'ok',
                'message' => 'Success',
                'file' => [
                    'user_id' => $screenshot->getUserId(),
                    'uniqid' => $screenshot->getUniqid(),
                    'size' => $screenshot->getSize(),
                    'created_at' => $screenshot->getCreatedAt(),
                    'display_link' => 'https://'.$_SERVER['HTTP_HOST'].'/display/'.$screenshot->getUniqid().'.png',
                ],
            ];
        }else{
            $api_show = [
                'status' => 'error',
                'message' => 'An error has occured when we try to move file',
            ];
        }
    }else{
        $api_show = [
            'status' => 'error',
            'message' => 'No file',
        ];
    }
}else{
    $api_show = [
        'status' => 'error',
        'message' => 'No file',
    ];
}

WxApi_Api::show($api_show);