<?php
/**
 * Project: auto-capture-site
 * Created by: WinXaito
 * Date: 10.04.2017
 */

require_once __DIR__.'/../init.php';

Wx_Session::requireApiAuthentication();

if(isset($_GET['name'])){
    $user = Wx_UserManager::getUserByName($_GET['name']);

    if($user == null)
        WxApi_Api::setAndShowError(404);

    if($user->getId() != Wx_Session::getUser()->getId())
        WxApi_Api::setAndShowError(403);

    $data = [
        'id' => $user->getId(),
        'name' => $user->getName(),
        'isAdministrator' => $user->isAdministrator(),
    ];
}else{
    Wx_Session::requireApiAdministrator();

    $users = Wx_UserManager::getAllUsers();
    $data = null;

    /** @var Wx_User $user */
    foreach($users as $user){
        $data[] = [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'isAdministrator' => $user->isAdministrator(),
        ];
    }
}

WxApi_Api::show($data);