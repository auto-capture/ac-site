<?php
/**
 * Project: auto-capture-site
 * Created by: WinXaito
 * Date: 09.04.2017
 */

/** @var $env array */

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../env.php';

if($env['env'] == 'prod')
    require_once __DIR__.'/../config.prod.php';
else
    require_once __DIR__.'/../config.dev.php';

if(isset($_GET['output']))
    WxApi_Api::setOutput($_GET['output']);
elseif(isset($_POST['output']))
    WxApi_Api::setOutput($_POST['output']);

if(isset($_POST['username'])) {
    $user = Wx_UserManager::getUserByName($_POST['username']);

    if($user != null)
        Wx_Session::init($user);
}

define('CURRENT_LINK', 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);